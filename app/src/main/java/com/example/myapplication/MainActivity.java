package com.example.myapplication;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class MainActivity extends AppCompatActivity {
    private static final int CAMERA_PERMISSION_CODE = 100;
    private static final int STORAGE_PERMISSION_CODE = 101;
    static TextView text;
    static File ScanFile;
    static String scanFilePath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, STORAGE_PERMISSION_CODE);
        checkPermission(Manifest.permission.CAMERA, CAMERA_PERMISSION_CODE);

        setContentView(R.layout.activity_main);
        Button btn = findViewById(R.id.btn_scan);
        text = findViewById(R.id.textView);
        text.setMovementMethod(ScrollingMovementMethod.getInstance());

        text.setText("");
        File sdcardDir = Environment.getExternalStorageDirectory();
        scanFilePath = "/ebutton_scan.txt";
        scanFilePath = sdcardDir.getPath() + scanFilePath;
        text.setText(scanFilePath + "\n");
        ScanFile = new File(scanFilePath);

        boolean exists = ScanFile.exists();
        try {
            if (exists) {
                Scanner myReader = new Scanner(ScanFile);
                while (myReader.hasNextLine()) {
                    String data = myReader.nextLine();
                    text.append(data + "\n");
                }
            } else {
                ScanFile.createNewFile();
            }
        } catch (IOException e) {
            // I am not going to make warning user and deal error in this code.
            // I already check file exist at the beginning.
            // because this just a quick project
            e.printStackTrace();
        }

        IntentIntegrator scanWindow = new IntentIntegrator(this);
        btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                scanWindow.initiateScan();
            }
        });


    }

    //  // `this` is the current Activity

    // Get the results:
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            if (result.getContents() == null) {
                Toast.makeText(this, "Cancelled", Toast.LENGTH_LONG).show();
            } else {

                Toast.makeText(this, "Scanned: " + result.getContents(), Toast.LENGTH_LONG).show();
                DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
                LocalDateTime now = LocalDateTime.now();
                String resultString = result.getContents() + ", " + dtf.format(now) + "\n\n";
                text.append(resultString);
                // open file and append content
                Path path = Paths.get(scanFilePath);
                try {
                    Files.write(path, resultString.getBytes(), StandardOpenOption.APPEND);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void checkPermission(String permission, int requestCode) {
        if (ContextCompat.checkSelfPermission(MainActivity.this, permission) == PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{permission}, requestCode);
        } else {
            Toast.makeText(MainActivity.this, "Permission already granted", Toast.LENGTH_SHORT).show();
        }

    }
}
